package com.phoenix.mypricekart.Fragment;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.phoenix.mypricekart.Api.Api;
import com.phoenix.mypricekart.Api.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SigninPresrenter implements Contract.Presenter {

    Contract.View view;
    @Override
    public void setView(Contract.View view) {
        this.view=view;
    }

    @Override
    public void getdetail(String email, String password) {


        Api api = ApiClient.getClient().create(Api.class);


        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("username", email)
                .addFormDataPart("password", password)
                .build();


        Call<SignInModel> call = api.LoginAi(requestBody);
        call.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {



                if (response.isSuccessful())
                {



                    view.getDetails(response.body());
                    Log.e( "onResponse: ",response.body().toString() );
                }






            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                Log.e( "onResponse: ",t.getMessage() );
            }
        });


    }

    @Override
    public void register(String email, String password) {




        Api api = ApiClient.getClient().create(Api.class);


        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("username", "null")
                .addFormDataPart("password", password)
                .addFormDataPart("email", email)
                .build();


        Call<JsonElement> call = api.RegistationCALL(requestBody);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {



                if (response.isSuccessful())
                {


                    try {
                        JSONObject jobj = new JSONObject(response.body().toString());
                            String error=jobj.getString("error");
                            String msg=jobj.getString("message");

                              view.regresult(error,msg);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    Log.e( "onResponse: ",response.body().toString() );
                }






            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.e( "onResponse: ",t.getMessage() );
            }
        });



    }
}
