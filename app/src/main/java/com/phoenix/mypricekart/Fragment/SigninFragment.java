package com.phoenix.mypricekart.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.phoenix.mypricekart.Activity.LoginActivity;
import com.phoenix.mypricekart.Activity.MainActivity;
import com.phoenix.mypricekart.R;
import com.phoenix.mypricekart.Utlits.AppPreference;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**

 */
public class SigninFragment extends Fragment implements Contract.View{
    // TODO: Rename parameter arguments, choose names that match
    Unbinder unbinder;
    @BindView(R.id.buttond)
    FrameLayout login;


    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.password)
    EditText password;
  SigninPresrenter signinPresrenter;
    AppPreference appPreference;
    SweetAlertDialog pDialog;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        signinPresrenter=new SigninPresrenter();
        signinPresrenter.setView(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, rootView);

         appPreference=new AppPreference(getContext());


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!isValidEmailId(email.getText().toString())) {
                    if (email.getText().toString().length() == 0) {
                        Toast.makeText(getContext(), "Enter Email", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Enter Valid Email", Toast.LENGTH_SHORT).show();
                    }
                    return;
                } else if (!isValidPassword(password.getText().toString())) {

                    if (password.getText().toString().length() == 0) {
                        Toast.makeText(getContext(), "Enter Password", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Enter Valid Password", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }else {
                 showProgress();
                signinPresrenter.getdetail(email.getText().toString(),password.getText().toString());


                }




            }
        });


        return rootView;
    }
    private boolean isValidEmailId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    private boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "[a-zA-Z0-9\\!\\@\\#\\$]{1,24}"
        );
        return !TextUtils.isEmpty(s) && PASSWORD_PATTERN.matcher(s).matches();
    }

    @Override
    public void getDetails(SignInModel success) {
        try {
            pDialog.dismissWithAnimation();

        }catch (Exception e)
        {

        }

        if (success.getError().booleanValue())
        {

            Snackbar.make(login,success.getMessage(), Snackbar.LENGTH_SHORT)
                    .show();
        }else {
            Snackbar.make(login,"successfully login", Snackbar.LENGTH_SHORT)
                    .show();

              appPreference.setLoginStatus("true");

            Intent i = new Intent(getContext(), MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);


        }
    }

    @Override
    public void regresult(String err, String msg) {

    }


    private void showProgress() {
        try {
            pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);

            pDialog.getProgressHelper().setBarColor(Color.parseColor("#F37D1F"));
            pDialog.getProgressHelper().setSpinSpeed(4);
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(true);
            pDialog.show();
        } catch (Exception e) {

        }
    }
}
