package com.phoenix.mypricekart.Fragment;

public class Contract {


    interface View {

        void getDetails(SignInModel success);
         void regresult(String err,String msg);

    }

    interface Presenter {

        void setView(View view);
        void getdetail(String email,String password);
        void register(String email,String password);
    }
}
