package com.phoenix.mypricekart.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.phoenix.mypricekart.Adapter.BidAdapter;
import com.phoenix.mypricekart.Adapter.SliderAdapterExample;
import com.phoenix.mypricekart.R;
import com.phoenix.mypricekart.callbacks.BackpressFromFragment;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ProductDetailFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.imageSlider)
    SliderView sliderView;
//    @BindView(R.id.recycler)
//    RecyclerView recyclerView;
//
//    @BindView(R.id.recycler1)
//    RecyclerView bidlist;

    BackpressFromFragment backpressFromFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            backpressFromFragment = (BackpressFromFragment) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement FragmentToActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        backpressFromFragment = null;


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_product_detail, container, false);
        unbinder = ButterKnife.bind(this, rootView);


        SliderAdapterExample adapter = new SliderAdapterExample(getContext());

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();

//
//
//        recyclerView.setHasFixedSize(true);
//
//        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setNestedScrollingEnabled(true);
//
//
//
//
//        bidlist.setHasFixedSize(true);
//        bidlist.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
//        bidlist.setHasFixedSize(true);



//        BidAdapter cashierAdapter1 = new BidAdapter( getContext());
//        bidlist.setAdapter(cashierAdapter1);

//        OrdersAdapter cashierAdapter = new OrdersAdapter( getContext());
//        recyclerView.setAdapter(cashierAdapter);

        return rootView;
    }





}
