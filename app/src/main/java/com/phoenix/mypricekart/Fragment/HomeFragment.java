package com.phoenix.mypricekart.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.phoenix.mypricekart.Adapter.BidAdapter;
import com.phoenix.mypricekart.Adapter.OrdersAdapter;
import com.phoenix.mypricekart.R;
import com.phoenix.mypricekart.Adapter.SliderAdapterExample;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class HomeFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.imageSlider)
    SliderView sliderView;
//    @BindView(R.id.recycler)
//    RecyclerView recyclerView;

    @BindView(R.id.recycler1)
    RecyclerView bidlist;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, rootView);


        SliderAdapterExample adapter = new SliderAdapterExample(getContext());

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();



//        recyclerView.setHasFixedSize(true);
//
//        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setNestedScrollingEnabled(false);




        bidlist.setHasFixedSize(true);
        bidlist.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        bidlist.setHasFixedSize(true);



        BidAdapter cashierAdapter1 = new BidAdapter( getContext());
        bidlist.setAdapter(cashierAdapter1);

//        OrdersAdapter cashierAdapter = new OrdersAdapter( getContext());
//        recyclerView.setAdapter(cashierAdapter);

        return rootView;
    }



}
