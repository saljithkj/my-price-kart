package com.phoenix.mypricekart.Activity;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuAdapter;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gitonway.lee.niftymodaldialogeffects.lib.Effectstype;
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder;
import com.google.android.material.tabs.TabLayout;
import com.phoenix.mypricekart.Fragment.HomeFragment;
import com.phoenix.mypricekart.R;
import com.phoenix.mypricekart.Utlits.AppConstants;
import com.phoenix.mypricekart.Utlits.AppPreference;
import com.phoenix.mypricekart.Utlits.RecyclerItemClickListener;
import com.phoenix.mypricekart.callbacks.BackpressFromFragment;
import com.phoenix.mypricekart.callbacks.UpdateTitle;
import com.shrikanthravi.customnavigationdrawer2.data.MenuItem;
import com.shrikanthravi.customnavigationdrawer2.widget.SNavigationDrawer;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements BackpressFromFragment, UpdateTitle {
    Class fragmentClass;
    public static Fragment fragment;
    SNavigationDrawer sNavigationDrawer;
    boolean doubleBackToExitPressedOnce = false;
    private boolean isFromBackPress = false;
    NiftyDialogBuilder materialDesignAnimatedDialog;


    @BindView(R.id.toolbar_main)
    Toolbar toolbarMain;
    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    @BindView(R.id.bottomNavigation)
    TabLayout tabLayout;
    @BindView(R.id.rv_menu)
    RecyclerView rvMenu;
    @BindView(R.id.drawerlayout)
    DrawerLayout drawerlayout;


    private ArrayList<MenuModel> MenuList = new ArrayList<>();
    private ActionBarDrawerToggle mDrawerToggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setSupportActionBar(toolbarMain);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        materialDesignAnimatedDialog = NiftyDialogBuilder.getInstance(this);

        toolbarMain.setTitleTextColor(getResources().getColor(android.R.color.white));
        setUpMenu();
        setUpTabViews();
        setDrawerState(true);
        replaceFragment(new HomeFragment(), "HOME HOME", 0, false);
        setToolBarTitle("Home");
        setSelected(0);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (!isFromBackPress) {
                    if (tab.getPosition() == 0) {
                        setSelected(0);
                        replaceFragment(new HomeFragment(), "HOME HOME", 0, true);
                        setToolBarTitle("Home");

                    } else if (tab.getPosition() == 1) {
                        setSelected(1);
//                        CreditFragment creditFragment = new CreditFragment();
//                        replaceFragment(creditFragment, "CREDIT CREDIT", 1, true);
//                        setToolBarTitle("All Vechicles");

                    } else if (tab.getPosition() == 2) {
                        setSelected(2);
//                        StaffFragment staffFragment = new StaffFragment();
//                        replaceFragment(staffFragment, "STAFF STAFF", 2, true);
//                        setToolBarTitle("All Staffs");

                    } else if (tab.getPosition() == 3) {
                        setSelected(3);
//                        replaceFragment(new TodolistMainFragment(), "TODOLIST TODOLIST", 3, true);
//                        setToolBarTitle("To Do List");

                    }
                    setDrawerState(true);
                }

                isFromBackPress = false;
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

                if (!isFromBackPress) {
                    if (tab.getPosition() == 0) {
                        setSelected(0);
                        replaceFragment(new HomeFragment(), "HOME HOME", 0, true);
                        setToolBarTitle("HOme");

                    } else if (tab.getPosition() == 1) {
                        setSelected(1);
//                        CreditFragment creditFragment = new CreditFragment();
//                        replaceFragment(creditFragment, "CREDIT CREDIT", 1, true);
//                        setToolBarTitle("All Vechicles");

                    } else if (tab.getPosition() == 2) {
                        setSelected(2);
//                        StaffFragment staffFragment = new StaffFragment();
//                        replaceFragment(staffFragment, "STAFF STAFF", 2, true);
//                        setToolBarTitle("All Staffs");


                    } else if (tab.getPosition() == 3) {
                        setSelected(3);
//                        replaceFragment(new TodolistMainFragment(), "TODOLIST TODOLIST", 3, true);
//                        setToolBarTitle("To Do List");

                    }
                    setDrawerState(true);
                }

                isFromBackPress = false;
            }
        });



    }



    private void setUpTabViews() {

        tabLayout.addTab(tabLayout.newTab().setCustomView(getTabView(0)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(getTabView(1)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(getTabView(2)));
        tabLayout.addTab(tabLayout.newTab().setCustomView(getTabView(3)));


        View root = tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(getResources().getColor(R.color.divider));
            drawable.setSize(0, 1);
            ((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }
    }
    public View getTabView(int position) {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tv = v.findViewById(R.id.txv_name);
        ImageView img = v.findViewById(R.id.imv_icon);


        switch (position) {
            case 0:
                tv.setText("HOME");
                img.setImageResource(R.drawable.menuhomeicon);
                break;

            case 1:

                tv.setText("TRACK");
                img.setImageResource(R.drawable.menucrediticon);
                break;

            case 2:

                tv.setText("WINNERS");
                img.setImageResource(R.drawable.menuiconstaff);
                break;

            case 3:

                tv.setText("ORDERS");
                img.setImageResource(R.drawable.menuloyalityicon);
                break;

        }


        return v;
    }
    public void setSelected(int selectedPosition) {

        switch (selectedPosition) {
            case 0:
                TextView title_terminal = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_terminal = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.imv_icon);
                imageView_terminal.setImageResource(R.drawable.menuhomeicon);
                title_terminal.setTextColor(getResources().getColor(R.color.white));

                TextView title_credit = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_credit = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.imv_icon);
                title_credit.setTextColor(getResources().getColor(R.color.black));
                imageView_credit.setImageResource(R.drawable.menuiconcredit);

                TextView title_wallet = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_wallet = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.imv_icon);
                title_wallet.setTextColor(getResources().getColor(R.color.black));
                imageView_wallet.setImageResource(R.drawable.menuiconstaff);


                TextView title_todo = tabLayout.getTabAt(3).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_todo = tabLayout.getTabAt(3).getCustomView().findViewById(R.id.imv_icon);
                title_todo.setTextColor(getResources().getColor(R.color.black));
                imageView_todo.setImageResource(R.drawable.menuloyalityicon);

                break;
            case 1:
                TextView title_terminal1 = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_terminal1 = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.imv_icon);
                imageView_terminal1.setImageResource(R.drawable.menuhomeicon);
                title_terminal1.setTextColor(getResources().getColor(R.color.black));

                TextView title_credit1 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_credit1 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.imv_icon);
                title_credit1.setTextColor(getResources().getColor(R.color.white));
                imageView_credit1.setImageResource(R.drawable.menuiconcredit);

                TextView title_wallet1 = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_wallet1 = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.imv_icon);
                title_wallet1.setTextColor(getResources().getColor(R.color.black));
                imageView_wallet1.setImageResource(R.drawable.menuiconstaff);


                TextView title_todo1 = tabLayout.getTabAt(3).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_todo1 = tabLayout.getTabAt(3).getCustomView().findViewById(R.id.imv_icon);
                title_todo1.setTextColor(getResources().getColor(R.color.black));
                imageView_todo1.setImageResource(R.drawable.menuloyalityicon);
                break;
            case 2:

                TextView title_terminal2 = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_terminal2 = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.imv_icon);
                imageView_terminal2.setImageResource(R.drawable.menuhomeicon);
                title_terminal2.setTextColor(getResources().getColor(R.color.black));

                TextView title_credit2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_credit2 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.imv_icon);
                title_credit2.setTextColor(getResources().getColor(R.color.black));
                imageView_credit2.setImageResource(R.drawable.menuiconcredit);

                TextView title_wallet2 = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_wallet2 = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.imv_icon);
                title_wallet2.setTextColor(getResources().getColor(R.color.white));
                imageView_wallet2.setImageResource(R.drawable.menuiconstaff);


                TextView title_todo2 = tabLayout.getTabAt(3).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_todo2 = tabLayout.getTabAt(3).getCustomView().findViewById(R.id.imv_icon);
                title_todo2.setTextColor(getResources().getColor(R.color.black));
                imageView_todo2.setImageResource(R.drawable.menuloyalityicon);
                break;
            case 3:

                TextView title_terminal3 = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_terminal3 = tabLayout.getTabAt(0).getCustomView().findViewById(R.id.imv_icon);
                imageView_terminal3.setImageResource(R.drawable.menuhomeicon);
                title_terminal3.setTextColor(getResources().getColor(R.color.black));

                TextView title_credit3 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_credit3 = tabLayout.getTabAt(1).getCustomView().findViewById(R.id.imv_icon);
                title_credit3.setTextColor(getResources().getColor(R.color.black));
                imageView_credit3.setImageResource(R.drawable.menuiconcredit);

                TextView title_wallet3 = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_wallet3 = tabLayout.getTabAt(2).getCustomView().findViewById(R.id.imv_icon);
                title_wallet3.setTextColor(getResources().getColor(R.color.black));
                imageView_wallet3.setImageResource(R.drawable.menuiconstaff);


                TextView title_todo3 = tabLayout.getTabAt(3).getCustomView().findViewById(R.id.txv_name);
                ImageView imageView_todo3 = tabLayout.getTabAt(3).getCustomView().findViewById(R.id.imv_icon);
                title_todo3.setTextColor(getResources().getColor(R.color.white));
                imageView_todo3.setImageResource(R.drawable.menuloyalityicon);
                break;


        }

    }

    public void setDrawerState(boolean isEnabled) {
        if (isEnabled) {
            drawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            mDrawerToggle.syncState();
//            txvUserName.setVisibility(View.VISIBLE);
//            ownerimage.setVisibility(View.VISIBLE);

        } else {
            drawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
//            txvUserName.setVisibility(View.GONE);
//            ownerimage.setVisibility(View.GONE);
            mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            mDrawerToggle.syncState();
        }
    }

    void setToolBarTitle(String title) {



        getSupportActionBar().setTitle(title);


    }

    private void replaceFragment(Fragment fragment, String TAG, int selectedPosition, boolean addtoBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container, fragment);

        if (addtoBackStack)
            transaction.addToBackStack(TAG);

        transaction.commit();


        // setSelected(selectedPosition);
    }

    @SuppressLint("RestrictedApi")
    void setUpMenu() {
        MenuModel remainder = new MenuModel("Order", R.drawable.bell);
        MenuModel currencyCalculator = new MenuModel("Account", R.drawable.menuiconcalculator);
        MenuModel supportRequest = new MenuModel("Support Requests", R.drawable.menuiconsuport);
        MenuModel settings = new MenuModel("Settings", R.drawable.menuiconsettings);
        MenuModel logout = new MenuModel("Logout", R.drawable.menuiconlogout);
        MenuModel notification = new MenuModel("Notifications", R.drawable.menunotificationicon);
//        MenuModel crm_login = new MenuModel("Dashboard", R.drawable.menuiconcrmlogin);

        MenuList.add(notification);
//        MenuList.add(crm_login);

        MenuList.add(currencyCalculator);
        MenuList.add(remainder);
        MenuList.add(supportRequest);
        MenuList.add(settings);
        MenuList.add(logout);


        rvMenu.setLayoutManager(new LinearLayoutManager(this));
        rvMenu.setAdapter(new com.phoenix.mypricekart.Adapter.MenuAdapter(this, MenuList));

        rvMenu.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (drawerlayout.isDrawerOpen(Gravity.LEFT))
                    drawerlayout.closeDrawer(Gravity.LEFT);
                else drawerlayout.openDrawer(Gravity.LEFT);

                switch (position) {


                    case 0:

//                        setTitle("Account Details");
//                        FragmentManager fragmentManageraccount = getSupportFragmentManager();
//                        FragmentTransaction fragmentTransactionaccount = fragmentManageraccount.beginTransaction();
//                        fragmentTransactionaccount.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
//                        fragmentTransactionaccount.replace(R.id.main_container, new AccountFragment());
//                        fragmentTransactionaccount.addToBackStack("HOME Account Details");
//                        fragmentTransactionaccount.commit();
//                        setSelected(0);
//                        updateSelectedTabTo(0);
//                        break;

                    case 1:

//                        Intent inotify = new Intent(MainActivity.this, NotificationActivity.class);
//                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
//                        startActivity(inotify);
                        break;

                    case 2:
//                        setTitle("Currency Calculator");
//                        FragmentManager fragmentManager1sms = getSupportFragmentManager();
//                        FragmentTransaction fragmentTransactionsms = fragmentManager1sms.beginTransaction();
//                        fragmentTransactionsms.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
//                        fragmentTransactionsms.replace(R.id.main_container, new CurrencyCalculatorFragment());
//                        fragmentTransactionsms.addToBackStack("HOME Currency Calculator");
//                        fragmentTransactionsms.commit();
//                        setSelected(0);
//                        updateSelectedTabTo(0);
                        break;


                    case 3:
//                        setTitle("Reminders");
//                        FragmentManager fragmentManagerremainder = getSupportFragmentManager();
//                        FragmentTransaction fragmentremainder = fragmentManagerremainder.beginTransaction();
//                        fragmentremainder.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
//                        fragmentremainder.replace(R.id.main_container, new RemainderFragment());
//                        fragmentremainder.addToBackStack("HOME Reminders");
//                        fragmentremainder.commit();
//                        setSelected(0);
//                        updateSelectedTabTo(0);
                        break;


//
//
//                    case 2:
//                        Thread loadingThread = new Thread() {
//
//                            @Override
//                            public void run() {
//                                try {
//                                    super.run();
//                                    sleep(300);
//                                } catch (Exception e) {
//
//                                } finally {
//
//                                    Intent i = new Intent(MainActivity.this, CrmLoginActivity.class);
//                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
//                                    startActivity(i);
//                                }
//                            }
//                        };
//                        loadingThread.start();
//
//                        break;
                    case 4:
//                        setTitle("Support Requests");
//                        FragmentManager fragmentManagerChecqueList = getSupportFragmentManager();
//                        FragmentTransaction transactionChecqueList = fragmentManagerChecqueList.beginTransaction();
//                        transactionChecqueList.replace(R.id.main_container, new SupportRequestFragment());
//                        transactionChecqueList.addToBackStack("HOME Support Requessts");
//                        transactionChecqueList.commit();
//                        setSelected(0);
//                        updateSelectedTabTo(0);
                        break;

                    case 6:
                       logoutalert();
                        break;

                }


            }
        }));
        setupDrawerToggle();
    }

    void updateSelectedTabTo(int position) {
        try {
            isFromBackPress = true;
            tabLayout.getTabAt(position).select();
        } catch (Exception e) {

        }


    }
    void setupDrawerToggle() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerlayout, toolbarMain, R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();

        drawerlayout.addDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                //Called when a drawer's position changes.

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                // Called when a drawer has settled in a completely closed state.
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                // Called when the drawer motion state changes. The new state will be one of STATE_IDLE, STATE_DRAGGING or STATE_SETTLING.
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                getCurrentFocus().clearFocus();
                if (AppConstants.isProfileUpdated) {
                    if (rvMenu.getAdapter() != null) {
                        rvMenu.getAdapter().notifyDataSetChanged();
                        AppConstants.isProfileUpdated = false;
                    }
                }

            }
        });
    }











    private void logoutalert() {

        materialDesignAnimatedDialog
                .withTitle("My Price Kart")
                .withMessage("Are you sure you want to exit?")
                .withDialogColor("#14151a")
                .withTitleColor("#FFFFFF")
                .withDividerColor("#11000000")
                .withMessageColor("#FFFFFFFF")
                .withButton1Text("NO")
                .withButton2Text("YES")
                .withDuration(500)

                .withEffect(Effectstype.Sidefill)
                .setButton2Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        AppPreference appPreference=new AppPreference(MainActivity.this);
                        appPreference.setLoginStatus("true");

                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);


                    }
                })
                .setButton1Click(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        materialDesignAnimatedDialog.dismiss();
                    }
                })

                .show();

    }


    @Override
    public void onBackPressed() {



            if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
              //  UiUtils.hideKeyboard(this, mainContainer);
                super.onBackPressed();

                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    String tempTag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
                    String[] splited = tempTag.split("\\s+");
                    String fragmentTag = splited[0];


                    if (fragmentTag.equals("HOME")) {

                      // setSelected(0);
                      //  updateSelectedTabTo(0);
//
//                        if (splited[0].equals(splited[1])) {
//                            setToolBarTitle(appPreference.getProfileName());
//                            setDrawerState(true);
//
//                        } else {
//                            setToolBarTitle(getToolBarTitle(splited));
//                            setDrawerState(false);
//                        }


                    } else if (fragmentTag.equals("CREDIT")) {
//                        setSelected(1);
//                        updateSelectedTabTo(1);
//
//                        if (splited[0].equals(splited[1])) {
//                            setToolBarTitle("All Credit Customers");
//
//                            setDrawerState(true);
//                        } else {
//
//                            setToolBarTitle(getToolBarTitle(splited));
//                            setDrawerState(false);
//                        }


                    } else if (fragmentTag.equals("STAFF")) {
//                        setSelected(2);
//                        updateSelectedTabTo(2);
//
//                        if (splited[0].equals(splited[1])) {
//
//                            setToolBarTitle("All Staffs");
//                            setDrawerState(true);
//
//                        } else {
//
//                            setToolBarTitle(getToolBarTitle(splited));
//                            setDrawerState(false);
//
//                        }
                        // UiUtils.hideKeyboard(this, mainContainer);

                    } else if (fragmentTag.equals("NEWS")) {
//                        setSelected(3);
//                        updateSelectedTabTo(3);
//
//                        if (splited[0].equals(splited[1])) {
//
//                            setToolBarTitle("News");
//                            setDrawerState(true);
//                        } else {
//
//                            setToolBarTitle(getToolBarTitle(splited));
//                            setDrawerState(false);
//
//                        }


                    } else if (fragmentTag.equals("TODOLIST")) {
//                        setSelected(4);
//                        updateSelectedTabTo(4);
//
//                        if (splited[0].equals(splited[1])) {
//
//                            setToolBarTitle("To Do List");
//                            setDrawerState(true);
//
//
//                        } else {
//
//                            setToolBarTitle(getToolBarTitle(splited));
//                            setDrawerState(false);
//                        }
                        // UiUtils.hideKeyboard(this, mainContainer);
                    }
                } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
//                    setSelected(0);
//                    updateSelectedTabTo(0);
//
//                    setToolBarTitle("HOME");
//                    setDrawerState(true);
                    //UiUtils.hideKeyboard(this, mainContainer);


                }
            }


    }

    @Override
    public void backPress() {


        onBackPressed();
    }

    @Override
    public void setTitle(String title) {



    }
}
