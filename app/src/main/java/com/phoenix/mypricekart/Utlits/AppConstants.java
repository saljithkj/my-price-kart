package com.phoenix.mypricekart.Utlits;



public class AppConstants {

    public static String baseURL = "https://crm.myluckymeter.com/admin/api/";


    public static int vehicleSelectedTickMark;
    public static int vehicleNumberSelectedTickMark;

    public static String NAME_KEY = "Name";
    public static String EMAIL_KEY = "Id";

    public static String Id_KEY = "Id";

    public static String LOGINSTATUS_KEY = "LoginStatus";
    public static String CUSTOMER_NAME = "cus_name";
    public static String CUSTOMER_LOGO = "logo";
    public static boolean isFragmentCreatedhome = false;
    public static boolean isFragmentCreatedcredit = false;
    public static boolean isFragmentCreatedbill = false;
    public static boolean isFragmentCreatedstaff = false;
    public static boolean isFragmentCreatedNews = false;
    public static boolean isFragmentCreatedTodolist = false;

    public static boolean isFragmentCreatedtricks = false;
    public static boolean isFragmentCreatedunbilled = false;
    public static boolean isFragmentCreatedbestpractice = false;
    public static boolean isFragmentCreatedlubes = false;

    public static boolean isTerminalUpdated = false;


    public static boolean isDataFetched = false;

    public static String AssignQRcodeValue = "";

    public static int fuelTypeSelectedValue =0;
    public static boolean isProfileUpdated = false;


    public static boolean isCardListUpdated = false;

    public static int updatedPosition = -1;
    public static boolean ENDDUTY = false;


    public  static void clear(){
        isFragmentCreatedTodolist=false;
        isFragmentCreatedhome = false;
        isFragmentCreatedcredit= false;
        isFragmentCreatedbill = false;
        isFragmentCreatedstaff=false;
        isTerminalUpdated = false;
        isFragmentCreatedNews=false;

        isFragmentCreatedlubes=false;
        isFragmentCreatedtricks = false;
        isFragmentCreatedbestpractice=false;
        isFragmentCreatedunbilled=false;

        isDataFetched = false;
        AssignQRcodeValue = "";
        fuelTypeSelectedValue = 0;
        isProfileUpdated = false;
        isCardListUpdated = false;

        updatedPosition = -1;
        ENDDUTY = false;
    }
}
