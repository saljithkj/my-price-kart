package com.phoenix.mypricekart.Utlits;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ATS on 28-Aug-17.
 */

public class AppPreference {

    private String APP_PREFERENCE = "MyLuckyMeter";
    private SharedPreferences pref;
    private SharedPreferences.Editor mProfile_Editor;

    public AppPreference(Context context) {

        pref = context.getSharedPreferences(APP_PREFERENCE, MODE_PRIVATE);
        mProfile_Editor = pref.edit();
    }

    public void setEmail(String Email) {
        mProfile_Editor.putString(AppConstants.EMAIL_KEY, Email);
        mProfile_Editor.commit();
    }

    public String getEmail() {
        return pref.getString(AppConstants.EMAIL_KEY, "");
    }

    public void setName(String Name) {
        mProfile_Editor.putString(AppConstants.NAME_KEY, Name);
        mProfile_Editor.commit();
    }




    public String getName() {
        return pref.getString(AppConstants.NAME_KEY, "");
    }


    public void setLoginStatus(String id) {
        mProfile_Editor.putString(AppConstants.LOGINSTATUS_KEY, id);
        mProfile_Editor.commit();
    }

    public String getLoginStatus() {
        return pref.getString(AppConstants.LOGINSTATUS_KEY, "");
    }


    public void setId(String id) {
        mProfile_Editor.putString(AppConstants.Id_KEY, id);
        mProfile_Editor.commit();
    }

    public String getId() {
        return pref.getString(AppConstants.Id_KEY, "");
    }


    public void setCustomerName(String Email) {
        mProfile_Editor.putString(AppConstants.CUSTOMER_NAME, Email);
        mProfile_Editor.commit();
    }

    public String getCustomerName() {
        return pref.getString(AppConstants.CUSTOMER_NAME, "");
    }


    public void setCustomerLogo(String Email) {
        mProfile_Editor.putString(AppConstants.CUSTOMER_LOGO, Email);
        mProfile_Editor.commit();
    }

    public String getCustomerLogo() {
        return pref.getString(AppConstants.CUSTOMER_LOGO, "");
    }






}