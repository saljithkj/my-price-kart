package com.phoenix.mypricekart.Api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.phoenix.mypricekart.Fragment.SignInModel;


import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Api {



    @POST("loginapi.php")
    Call<SignInModel> LoginAi(@Body RequestBody jsonObject);



    @POST("registration.php")
    Call<JsonElement> RegistationCALL(@Body RequestBody jsonObject);


}