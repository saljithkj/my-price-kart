package com.phoenix.mypricekart.Adapter;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;

import com.mikhaellopez.hfrecyclerview.HFRecyclerView;
import com.phoenix.mypricekart.Activity.MenuModel;
import com.phoenix.mypricekart.R;
import com.phoenix.mypricekart.Utlits.AppPreference;
import com.phoenix.mypricekart.Utlits.GlideCircleTrasform;

import java.util.ArrayList;

public class MenuAdapter extends HFRecyclerView<MenuModel> {
    private Activity activity;
    private ArrayList<MenuModel> MenuList = new ArrayList<>();
    private AppPreference appPreference;


    public MenuAdapter(Activity activity, ArrayList<MenuModel> MenuList) {
        super(MenuList, true, false);
        this.activity = activity;
        this.MenuList = MenuList;
        this.appPreference = new AppPreference(activity);

    }

    @Override
    protected RecyclerView.ViewHolder getItemView(LayoutInflater inflater, ViewGroup parent) {
        return new ItemViewHolder(inflater.inflate(R.layout.menu_row_item, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getHeaderView(LayoutInflater inflater, ViewGroup parent) {
        return new HeaderViewHolder(inflater.inflate(R.layout.menu_header, parent, false));
    }

    @Override
    protected RecyclerView.ViewHolder getFooterView(LayoutInflater inflater, ViewGroup parent) {
        return new FooterViewHolder(inflater.inflate(R.layout.menu_footer, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            int listPosition = --position;
            itemViewHolder.title.setText(MenuList.get(listPosition).getTitle());

            itemViewHolder.image.setImageResource(MenuList.get(listPosition).getImage());


        } else if (holder instanceof HeaderViewHolder) {

            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            String buildnumber = String.valueOf(headerViewHolder.Buildnumber);
            String vfinal = "v" + headerViewHolder.versionName + "(" + buildnumber + ")";
            headerViewHolder.verion_name.setText(vfinal);

            headerViewHolder.mTxv_Username.setPaintFlags(headerViewHolder.mTxv_Username.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            String userName = appPreference.getCustomerName();
            if (!userName.isEmpty()) {
                String finalUserName = userName.substring(0, 1).toUpperCase() + userName.substring(1);

                headerViewHolder.mTxv_Username.setText(finalUserName);
            } else {
                headerViewHolder.mTxv_Username.setText("");
            }

            if (!appPreference.getCustomerLogo().isEmpty()) {
                String imUrl="https://crm.myluckymeter.com/admin/superadmin/uploads/"+appPreference.getCustomerLogo();
                Glide.with(activity).load(imUrl)
                        .apply(new RequestOptions()
                                .transforms(new CircleCrop(), new GlideCircleTrasform()))
                        .into(headerViewHolder.mImv_User);
            }
        } else if (holder instanceof FooterViewHolder) {

        }
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView image;

        public ItemViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.menu_item_name);
            image = itemView.findViewById(R.id.menu_item_image);
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {

        private TextView mTxv_Username, verion_name;
        private ImageView mImv_User;
        String versionName;
        int Buildnumber;


        public HeaderViewHolder(View itemView) {
            super(itemView);

            mTxv_Username = itemView.findViewById(R.id.txv_username);
            mImv_User = itemView.findViewById(R.id.imv_user_img);
            verion_name = itemView.findViewById(R.id.verion_name);

            try {

                Buildnumber = itemView.getContext().getPackageManager()
                        .getPackageInfo(itemView.getContext().getPackageName(), 0).versionCode;

                versionName = itemView.getContext().getPackageManager()
                        .getPackageInfo(itemView.getContext().getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }
}
